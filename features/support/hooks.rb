require "selenium-webdriver"
require "base64"
ENV["http_proxy"] = nil

Before do
  pagina = page.driver.browser.manage.window
  pagina.resize_to(1500, 1500)
  #pagina.maximize
  @commons = Commons.new
  #@cdc_met = Ficha_cdc_metodos.new
end

AfterStep do
  #binding.pry
  screenshot = save_screenshot("data/screenshots/#{Time.now.strftime("%Y%m%d%H%M%S")}.png")
  embed("data:image/png;base64,#{Base64.encode64(open(screenshot).to_a.join)}", "image/png")
end

After do
  #binding.pry
  screenshot = save_screenshot("data/screenshots/#{Time.now.strftime("%Y%m%d%H%M%S")}.png")
  embed("data:image/png;base64,#{Base64.encode64(open(screenshot).to_a.join)}", "image/png")
end
