class Commons
  include Capybara::DSL
  include RSpec::Matchers

  def preenche_formulario_cliente
    #   expect(find("#user-name").value).not_to eql(nil)
    find("#user-email").set("omni.aut@omni.com.br")
    find("#user-married").find("option[value='1']").select_option
    find("#user-id").find("option[value='4']").select_option
    find("input[name='numeroDocumentoCliente']").set("124589566")
    find("input[name='orgaoDocumentoCliente']").set("ssp")
    "12/03/2012".split("").each do |dig|
      find("#emiss-doc").native.send_keys(dig)
    end
    binding.pry
    find("#user-mother-name").click unless all("#user-mother-name").size == 0 
    find("#user-mon-other-name").set("Mae Automacao Omni")
    find("input[name='nomePaiCliente']").set("Pai automacao omni")
    find("#user-citizenship").find("option[value='1']").select_option
    find("#uf-nat-cli").find("option[value='SP']").select_option
    find("#user-city-cli").find("option[value='SAO PAULO']").select_option
    find("select[name='classeProfissional']").find("option", text: "EMPREGADO").select_option
    find("#user-occupation").find("option[value='3: Object']").select_option
    find("#user-work-company").set("Omni lero lero")
    find("input[name='foneEmpresaCliente']").set("1125874587")
    find("#user-patrimony").set("55987877")
    find("#user-reference").set("referencia san")
    find("#user-reference-phone").set("1145458787")
    find("#user-reference-relation").find("option[value='2']").select_option
  end

  def preenche_end_residencial
    "05780230".split("").each do |c|
      find("app-form-group[ng-reflect-icon='residencial']").find("#cep").native.send_keys(c)
    end
    find("app-ficha-cadastro-endereco-cliente").click
    sleep 3
    campos_preenchidos_end_residencial
  end

  def campos_preenchidos_end_residencial
    container = find("app-ficha-cadastro-endereco-cliente")
    expect(container.all("#endereco")[0].value).not_to eql ""
    container.all("input[name='numero']")[0].set("55")
  end

  def preenche_end_comercial
    "05780230".split("").each do |c|
      find("app-endereco[ng-reflect-name='enderecoComercial']").find("#cep").native.send_keys(c)
    end
    find("app-endereco[ng-reflect-name='enderecoComercial']").click
    sleep 3
    campos_preenchidos_end_comercial
  end

  def campos_preenchidos_end_comercial
    container = find("app-endereco[ng-reflect-name='enderecoComercial']")
    expect(container.all("#endereco")[0].value).not_to eql ""
    container.all("input[name='numero']")[0].set("55")
  end

  def confirma_dados_tel
    sleep 2
    find(:xpath, "//input[@name=\"P_PROCEDE_RES\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_RES\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_RES\"]").set("anon")
    # binding.pry
    find(:xpath, "//input[@name=\"P_CONTATO_RES\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_RES\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_RES\"]").set("anon")

    find(:xpath, "//input[@name=\"P_PROCEDE_COM\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_CONTATO_COM\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_COM\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_COM\"]").set("anon")

    find(:xpath, "//input[@name=\"P_PROCEDE_FAM1\"][2]").click
    find(:xpath, "//input[@name=\"P_EM_NOME_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_QUAL_END_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_CONTATO_FAM1\"]").set("anon")
    find(:xpath, "//input[@name=\"P_RELACAO_FAM1\"]").set("anon")
    find(:xpath, "//textarea[@name=\"P_INFORMACAO_FAM1\"]").set("anon")
  end

  def valida_alert
    begin
      retries ||= 0
      "vam ve"
      puts "#{page.driver.browser.switch_to.alert.text} SIM."
      page.accept_alert
      raise "PAGINA NAO RESPONDEU" unless assert_no_selector(".main-frame-error") == true
    rescue
      p "sera?"
      retry if (retries += 1) < 30
    end
  end

  def encontra_cpf
    conn = OCI8.new("supervisor", "omnidev", "10.20.1.111:1522/omnidev")
    query = "SELECT a.cpf_cgc,b.proposta
    FROM clientes_proposta a,
         retorno_neuro_server b 
    WHERE a.proposta = b.proposta
    AND pontuacao > 88 
    AND TRUNC(data_execucao) BETWEEN '01-jan-2015' AND Trunc(SYSDATE)
    AND cpf_cgc in (SELECT cpf_cgc FROM clientes WHERE cpf_cgc = A.cpf_cgc AND cliente_omni = 'S' )
    and ROWNUM <= 3"
    @teste = []
    cursor = conn.parse(query)
    cursor.exec
    cursor.fetch() { |row| @teste << row[0] }
    return @proposta = @teste[0]
  end

  def foca_tela()
    result = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(result)
  end
end
