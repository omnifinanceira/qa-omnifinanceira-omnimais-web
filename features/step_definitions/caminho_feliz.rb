Dado("que eu esteja no ambiente {string}") do |string|
  visit(MASSA[string])
end

Dado("eu entre com o usuario {string} e senha {string}") do |string, string2|
  find("#user-login").set(string)
  find("#user-password").set(string2)
  find(".btn.btn-login.btn-sign").click
end

Dado("eu esteja na tela inicial") do
  #assert_selectors(".record", wait: 15)
  assert_selector(".new-record", wait: 15)
end

Dado("eu clique no Botao {string}") do |string|
  find(ELEMENTOS[string]).click
end

Dado("eu selecione {string} como {string}") do |string, string2|
  sleep 0.2
  find(ELEMENTOS[string2]).click
end

Dado("eu clicar no botao {string}") do |string|
  sleep 0.3
  find(ELEMENTOS[string]).click
end

Dado("eu escolher {string} como {string}") do |string, string2|
  find(ELEMENTOS[string]).find("option[value='#{ELEMENTOS[string2]}']").select_option
end

Dado("eu escolher o vendedor") do
  pending # Write code here that turns the phrase above into concrete actions
end

Dado("eu digitar o cpf do cliente como {string}") do |string|
  string = string.split("")
  string.each do |dig|
    find("input[name='cpf-cliente']").native.send_keys(dig)
  end
  find(".icon.id-number").click
  sleep 3
  click_button("Iniciar nova ficha") unless all(".modal-dialog.modal-md").size < 1
end

Dado("eu preencher o campo {string} como {string}") do |string, string2|
  # find(ELEMENTOS[string]).set(string2)
  # binding.pry
  string2 = string2.split("")
  string2.each do |dig|
    find(ELEMENTOS[string]).native.send_keys(dig)
  end
end

Dado("eu selecionar o estado de licenciamento como {string}") do |string|
  find("#uf-licensing").find("option[value='#{string}']").select_option
end

Quando("eu aguardar o processamento") do
  binding.pry
  assert_no_selector(".loading-containner", wait: 30)
  assert_no_text("Fora da Política Interna")
end

Quando("eu espero visualizar as opcoes de financiamento") do
  click_button("Ok") unless all(".modal-content", wait: 7).size < 1
  puts find("veiculo p").text
  puts find("app-valor-financiado p").text
end

Quando("eu preencher os campos de detalhes do cliente") do
  ##binding.pry
  @commons.preenche_formulario_cliente
end

Quando("eu preencher os campos de endereco") do
  @commons.preenche_end_residencial
  @commons.preenche_end_comercial
  find("#user-mail-address").find("option[value='1']").select_option
end
Entao("eu espero visualizar a mensagem {string}") do |string|
  assert_text(string, wait: 180)
end
Quando("eu preecher o campo {string} como {string}") do |string, string2|
  find(ELEMENTOS[string]).set(string2)
end
Quando("eu possua uma ficha no status {string}") do |status|
  conn = OCI8.new("supervisor", "omnidev", "10.20.1.111:1522/omnidev")
  query = "SELECT DISTINCT LOCAL, status, proposta FROM proposta p
  WHERE EXISTS (SELECT 1 FROM rastreio_proposta r 
  WHERE r.proposta = p.proposta AND sequencia = (SELECT Max(sequencia) 
  FROM rastreio_proposta r2 WHERE r.proposta = r2.proposta 
  ) 
  AND cod_status = #{status}
  ) 
  AND emissao BETWEEN SYSDATE - 15 AND SYSDATE
  AND agente = '184'"
  @teste = []
  cursor = conn.parse(query)
  cursor.exec
  cursor.fetch() { |row| @teste << row[2] }
  puts @proposta = @teste[0].to_i
end

Quando("eu pesquisar pela ficha") do
  find("a", text: "Filtrar Fichas").click
  find(".row.filter-panel").find("input[name='numero']").set(@proposta)
  find("app-ficha-card").find(".head").click
end

Quando("eu selecionar a opcao RESULTADO PARCIAL na timeline da ficha") do
  find("app-ficha-timeline-resultado-parcial").find("div[ng-reflect-klass='timeline-panel']").click
end

Dado("eu possua uma ficha no status {string} para {string}") do |string, string2|
  conn = OCI8.new("supervisor", "omnidev", "10.20.1.111:1522/omnidev")
  query = "select p.* from proposta p
  where p.grupo1 = '#{string2}'  
  and p.proposta in (
    select proposta from rastreio_proposta r
      --where rownum <=1
      having max(r.cod_status) ='#{string}'
      group by r.proposta)
  and p.vendedor_lojista is not null 
  AND p.emissao BETWEEN SYSDATE - 15 AND SYSDATE
  order by proposta desc"
  @teste = []
  cursor = conn.parse(query)
  cursor.exec
  cursor.fetch() { |row| @teste << row[0] }
  puts @proposta = @teste[0].to_i
end

Dado("eu possua uma ficha no status Em analise para {string}") do |string|
  conn = OCI8.new("supervisor", "omnidev", "10.20.1.111:1522/omnidev")
  query = "select p.* from proposta p
  where p.grupo1 = '#{string}'
  and p.proposta in (
    select proposta from rastreio_proposta r
      --where rownum <=1
      having max(r.cod_status) =2
      group by r.proposta)
  and p.vendedor_lojista is not null 
  and p.agente = '184'
  AND p.emissao BETWEEN SYSDATE - 15 AND SYSDATE
  order by proposta desc"
  @teste = []
  cursor = conn.parse(query)
  cursor.exec
  cursor.fetch() { |row| @teste << row[0] }
  puts @proposta = @teste[0].to_i
end

Dado("eu possua uma ficha balcao no status {string} para {string}") do |string, string2|
  conn = OCI8.new("supervisor", "omnidev", "10.20.1.111:1522/omnidev")
  query = "select p.proposta from proposta p , rastreio_proposta r
  where p.lojista = '1184'
  and p.vendedor_lojista is null
  and p.grupo1 = '#{string2}'
  and r.proposta = p.proposta
  and r.cod_status = '#{string}'
  AND sequencia = (SELECT Max(sequencia) 
  FROM rastreio_proposta r2 WHERE r.proposta = r2.proposta 
  ) 
  AND p.emissao BETWEEN SYSDATE - 15 AND SYSDATE"
  @teste = []
  cursor = conn.parse(query)
  cursor.exec
  cursor.fetch() { |row| @teste << row[0] }
  puts @proposta = @teste[0].to_i
end

Quando("eu digitar o cpf do cliente") do
  #puts @cpf = "02022711882"#
  @cpf = @commons.encontra_cpf
  @cpf = @cpf.split ""
  @cpf.each do |dig|
    find("input[name='cpf-cliente']").native.send_keys(dig)
  end
  find(".icon.id-number").click
  sleep 3
  click_button("Iniciar nova ficha") unless all(".modal-dialog.modal-md").size < 1
end
