Dado("eu esteja logado com usuario {string} e senha {string}") do |string, string2|
  find("#p-nome", wait: 5).set(string)
  find("#p-senha", wait: 5).set(string2)
  click_button "Conectar"
end

Dado("eu esteja na fila") do
  find("a[url='pck_bs.prc_menu_bs']").click
  find("select[name='P_EMP']").find("option[value='184']").select_option
  click_button("Validar")
  operacional = find("#navbar-collapse-1").find(".dropdown", text: "OPERACIONAL").click
  operacional.assert_selector(".dropdown.dropdown-submenu")
  credito = operacional.find(".dropdown.dropdown-submenu", text: "CRÉDITO").click
  credito.all("li", text: "Fila")[0].click
end

Quando("eu selecionar a proposta") do
  within_frame("frm-principal") do
    within_frame("bottomFrame2") do
      #binding.pry
      @linha = all("tr", text: @proposta, wait: 30)[1]
      @linha.find("input[type='button']").click
    end
  end
end

Quando("eu aceito a Consulta Base Historica") do
  within_frame("frm-principal") do
    find(ELEMENTOS["Historica"]).click
    sleep 3
    find(ELEMENTOS["botao_aceitar"]).click
    @commons.valida_alert
    find(ELEMENTOS["Ficha_Verif"], wait: 10).click
    expect(find(ELEMENTOS["Historica"])["class"]).to eq("botaoVerde")
  end
end

Quando("eu aceito a ficha cadastral") do
  within_frame("frm-principal") do
    find("input[value='Gravar']").click
    @commons.valida_alert
    find(ELEMENTOS["botao_aceitar"]).click
    @commons.valida_alert
    assert_text("Proposta Pré-Aprovada pelo processo de Análise Automática")
  end
end

Quando("eu aceito a decisao") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("eu realizo o checklist") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("eu preencho o parecer final") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("eu preencho os dados do usuario com alcada") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("eu valido a proposta") do
  pending # Write code here that turns the phrase above into concrete actions
end

Entao("eu espero visualizar a proposta aprovada no Omni Mais") do
  pending # Write code here that turns the phrase above into concrete actions
end

Quando("eu envio a mesa") do
  within_frame("frm-principal") do
    find(ELEMENTOS["botao_mesa"]).click
    @commons.valida_alert
  end
end
Quando("eu seleciono o motivo") do
  within_frame("frm-principal") do
    linhas = find(:xpath, "//input[@value='000090_PROPOSTA TESTE']").click
    find("textarea[name='P_OBSERVACAO_AGENTE']").set("aprova ai")
  end
end

Quando("eu seleciono gravar") do
  within_frame("frm-principal") do
    click_button "Gravar"
    @commons.valida_alert
  end
end

Entao("eu espero que a proposta esteja na mesa") do
  @commons.foca_tela
  within_frame("frm-principal") do
    click_button "Gravar"
    @commons.valida_alert
    @commons.valida_alert
  end
end
