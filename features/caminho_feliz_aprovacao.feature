#language:pt
Funcionalidade: Aprovar ficha do Omni Mais

@10
Cenario: Enviar proposta pra mesa
Dado eu possua uma ficha no status Em analise para "CDC"
E que eu esteja no ambiente "bizfacil lucas"
E eu esteja logado com usuario "184batistella" e senha "senha123"
E eu esteja na fila
Quando eu selecionar a proposta
E eu aceito a Consulta Base Historica
E eu aceito a ficha cadastral
E eu envio a mesa
E eu seleciono o motivo 
E eu seleciono gravar
Entao eu espero que a proposta esteja na mesa


@11
Cenario: Aprovar uma proposta com sucesso
Dado eu possua uma ficha no status Em analise para "CDC"
E eu possuo uma proposta com status "DEVOLVIDA"
E eu esteja na tela de Mesa de credito
Quando eu seleciono a Mesa desejada
E eu seleciono a proposta na Fila da mesa
E eu aceito a decisao pela mesa
E eu realizo o checklist pela mesa
E eu preencho o parecer final pela mesa


