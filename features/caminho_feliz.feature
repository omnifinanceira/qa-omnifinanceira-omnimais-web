#language:pt
@simulador
Funcionalidade: Simulador de Ficha Cadastral
Contexto:
Dado que eu esteja no ambiente "Omni mais hmg"
E eu entre com o usuario "184TIAGO" e senha "senha123"
E eu esteja na tela inicial

@1
Cenario: cria uma ficha - VENDEDOR - FINANCIAMENTO
E eu clique no Botao "Adicionar ficha"
E eu selecione "Quem solicitou" como "VENDEDOR"
E eu selecione "Tipo de Operação" como "Financiamento"
Quando eu clicar no botao "Continuar"
E eu escolher "Loja do vendedor" como "AUTOVAN VEICULOS"
E eu escolher "Nome do Vendedor" como "GRASCIELY FREITAS GOMES"
E eu clicar no botao "Continuar"
E eu digitar o cpf do cliente
E eu preencher o campo "data de nascimento" como "11111990"
E eu preencher o campo "celular do cliente" como "45007916803"
E eu preencher o campo "renda comprovada" como "650000"
E eu clicar no botao "Continuar"
E eu preencher o campo "placa do veiculo" como "emh8494"
E eu selecionar o estado de licenciamento como "SP"
E eu clicar no botao "Continuar"
E eu aguardar o processamento
Entao eu espero visualizar as opcoes de financiamento

@2
Cenario: cria uma ficha - VENDEDOR - REFINANCIAMENTO
E eu clique no Botao "Adicionar ficha"
E eu selecione "Quem solicitou" como "VENDEDOR"
E eu selecione "Tipo de Operação" como "Re-financiamento"
Quando eu clicar no botao "Continuar"
E eu escolher "Loja do vendedor" como "AUTOVAN VEICULOS"
E eu escolher "Nome do Vendedor" como "GRASCIELY FREITAS GOMES"
E eu clicar no botao "Continuar"
E eu digitar o cpf do cliente
E eu preencher o campo "data de nascimento" como "11111990"
E eu preencher o campo "celular do cliente" como "45007916803"
E eu preencher o campo "renda comprovada" como "650000"
E eu clicar no botao "Continuar"
E eu preencher o campo "placa do veiculo" como "emh8494"
E eu selecionar o estado de licenciamento como "SP"
E eu clicar no botao "Continuar"
E eu aguardar o processamento
Entao eu espero visualizar as opcoes de financiamento

@3
Cenario: cria uma ficha - CLIENTE NO BALCAO - FINANCIAMENTO
E eu clique no Botao "Adicionar ficha"
E eu selecione "Quem solicitou" como "CLIENTE NO BALCAO"
E eu selecione "Tipo de Operação" como "Financiamento"
Quando eu clicar no botao "Continuar"
#E eu escolher "Loja do vendedor" como "AUTOVAN VEICULOS"
E eu escolher "Nome do Vendedor" como "Nao cadastrado"
E eu clicar no botao "Continuar"
E eu digitar o cpf do cliente
E eu preencher o campo "data de nascimento" como "11111990"
E eu preencher o campo "celular do cliente" como "45007916803"
E eu preencher o campo "renda comprovada" como "650000"
E eu clicar no botao "Continuar"
E eu preencher o campo "placa do veiculo" como "emh8494"
E eu selecionar o estado de licenciamento como "SP"
E eu clicar no botao "Continuar"
E eu aguardar o processamento
Entao eu espero visualizar as opcoes de financiamento

@4
Cenario: cria uma ficha - CLIENTE NO BALCAO - REFINANCIAMENTO
E eu clique no Botao "Adicionar ficha"
E eu selecione "Quem solicitou" como "CLIENTE NO BALCAO"
E eu selecione "Tipo de Operação" como "Re-financiamento"
Quando eu clicar no botao "Continuar"
#E eu escolher "Loja do vendedor" como "AUTOVAN VEICULOS"
E eu escolher "Nome do Vendedor" como "Nao cadastrado"
E eu clicar no botao "Continuar"
E eu digitar o cpf do cliente
E eu preencher o campo "data de nascimento" como "11111990"
E eu preencher o campo "celular do cliente" como "45007916803"
E eu preencher o campo "renda comprovada" como "650000"
E eu clicar no botao "Continuar"
E eu preencher o campo "placa do veiculo" como "emh8494"
E eu selecionar o estado de licenciamento como "SP"
E eu clicar no botao "Continuar"
E eu aguardar o processamento
Entao eu espero visualizar as opcoes de financiamento
####################################################################################
############################## PARTE 2 #############################################
####################################################################################

@5
Cenario: Complementar uma ficha com os dados do cliente - VENDEDOR - FINANCIAMENTO
E eu possua uma ficha no status "1" para "CDC"
Quando eu pesquisar pela ficha
E eu selecionar a opcao RESULTADO PARCIAL na timeline da ficha
E eu espero visualizar as opcoes de financiamento
E eu clicar no botao "Continuar"
E eu preencher os campos de detalhes do cliente
E eu clicar no botao "Continuar"
E eu preencher os campos de endereco
E eu clicar no botao "Continuar"
E eu preecher o campo "Observação" como "Testes automatizados"
E eu clicar no botao "Continuar"
Entao eu espero visualizar a mensagem "SOLICITAÇÃO FOI FINALIZADA"

@6
Cenario: Complementar uma ficha com os dados do cliente - VENDEDOR - REFINANCIAMENTO
E eu possua uma ficha no status "1" para "REFINANCIAMENTO"
Quando eu pesquisar pela ficha
E eu selecionar a opcao RESULTADO PARCIAL na timeline da ficha
E eu espero visualizar as opcoes de financiamento
E eu clicar no botao "Continuar"
E eu preencher os campos de detalhes do cliente
E eu clicar no botao "Continuar"
E eu preencher os campos de endereco
E eu clicar no botao "Continuar"
E eu preecher o campo "Observação" como "Testes automatizados"
E eu clicar no botao "Continuar"
Entao eu espero visualizar a mensagem "SOLICITAÇÃO FOI FINALIZADA"

@7
Cenario: Complementar uma ficha com os dados do cliente - BALCAO - FINANCIAMENTO
E eu possua uma ficha balcao no status "1" para "CDC"
Quando eu pesquisar pela ficha
E eu selecionar a opcao RESULTADO PARCIAL na timeline da ficha
E eu espero visualizar as opcoes de financiamento
E eu clicar no botao "Continuar"
E eu preencher os campos de detalhes do cliente
E eu clicar no botao "Continuar"
E eu preencher os campos de endereco
E eu clicar no botao "Continuar"
E eu preecher o campo "Observação" como "Testes automatizados"
E eu clicar no botao "Continuar"
Entao eu espero visualizar a mensagem "SOLICITAÇÃO FOI FINALIZADA"

@8
Cenario: Complementar uma ficha com os dados do cliente - BALCAO - REFINANCIAMENTO
E eu possua uma ficha balcao no status "1" para "REFINANCIAMENTO"
Quando eu pesquisar pela ficha
E eu selecionar a opcao RESULTADO PARCIAL na timeline da ficha
E eu espero visualizar as opcoes de financiamento
E eu clicar no botao "Continuar"
E eu preencher os campos de detalhes do cliente
E eu clicar no botao "Continuar"
E eu preencher os campos de endereco
E eu clicar no botao "Continuar"
E eu preecher o campo "Observação" como "Testes automatizados"
E eu clicar no botao "Continuar"
Entao eu espero visualizar a mensagem "SOLICITAÇÃO FOI FINALIZADA"








